package com.example.database;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

//Кастомный адаптер для RecView
public class RecViewAdapter extends RecyclerView.Adapter<RecViewAdapter.VH> {

    ArrayList<String> name, status, time, date;

    //Кастомный конструктор
    public RecViewAdapter (ArrayList<String> name, ArrayList<String> status, ArrayList<String> time, ArrayList<String> date) {
        this.date = date;
        this.name = name;
        this.status = status;
        this.time = time;
    }

    //Создаём новый объект типа VH и передаём ему view'шку нашего элемента
    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.database_item, parent, false));
    }

    //Настраиваем переменные в элементах
    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.name.setText(name.get(position));
        holder.status.setText(status.get(position));
        holder.date.setText(date.get(position));
        holder.time.setText(time.get(position));
    }

    //Кол-во элементов
    @Override
    public int getItemCount() {
        return date.size();
    }

    //Кастомный класс ViewHolder'а для RecView
    public class VH extends RecyclerView.ViewHolder {
        TextView name, status, date, time;
        public VH(@NonNull View itemView) {
            super(itemView);
            //Присваивание айдишек
            name = itemView.findViewById(R.id.hope);
            status = itemView.findViewById(R.id.status);
            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
        }
    }
}
