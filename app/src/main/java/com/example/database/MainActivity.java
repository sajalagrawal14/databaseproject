package com.example.database;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

//Главная активность
public class MainActivity extends AppCompatActivity {

    static DatabaseHelper helper;
    static SQLiteDatabase base;
    static ArrayList<String> name = new ArrayList<String>(), status = new ArrayList<String>(), date = new ArrayList<String>(), time = new ArrayList<String>(), second = new ArrayList<String>();
    static ContentValues values;
    static RecyclerView recyclerView;
    static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Настраиваем таблицу
        helper = new DatabaseHelper(this);
        try {
            helper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }
        try {
            base = helper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }

        recyclerView = findViewById(R.id.wall);
        context = this;

        //Записываем из второй таблицы все значения в отдельный массив
        Cursor cursor = base.rawQuery("SELECT * FROM second", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            second.add(cursor.getString(1));
            cursor.moveToNext();
        }
        cursor.close();

        //Вызываем кастомный метод.
        Method();
    }

    //При нажатии на кнопку "Сгенерировать желание"
    public void onClicked(View v) {
        //Вызываем кастомный AlertDialog
        FragmentManager manager = getSupportFragmentManager();
        AlertDialogBruh myDialogFragment = new AlertDialogBruh(second.get(new Random().nextInt(second.size())));
        myDialogFragment.show(manager, "myDialog");
    }

    //При нажатии на "Добавить" в диалоговом окне
    static void onItemClicked(String bruh) {
        //По идее, это запись данных в таблицу, но оно не работает :/
        values = new ContentValues();
        values.put(bruh, 1);
        values.put("00:00", 2);
        values.put("07.05.2020", 3);
        values.put("Выполняется", 4);

        // Вставляем новый ряд в базу данных и запоминаем его идентификатор
        long newRowId = base.insert("first", null, values);

        // Выводим сообщение в успешном случае или при ошибке
        if (newRowId == -1) {
            // Если ID  -1, значит произошла ошибка
            Toast.makeText(context, "Ошибка!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Гость заведён под номером: " + newRowId, Toast.LENGTH_SHORT).show();
        }

        //Вызываем кастомный метод.
        Method();
    }

    //Метод для обновления RecView и его значений
    static void Method() {
        //Стираем старые данные
        name.clear();
        status.clear();
        date.clear();
        time.clear();

        //Запись в переменные значений таблицы
        Cursor cursor = base.rawQuery("SELECT * FROM first", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            name.add(cursor.getString(1));
            date.add(cursor.getString(2));
            time.add(cursor.getString(3));
            status.add(cursor.getString(4));
            cursor.moveToNext();
        }
        cursor.close();

        //Зашружаем данные в RecView
        RecViewAdapter adapter = new RecViewAdapter(name, status, time, date);
        recyclerView.setAdapter(adapter);
    }
}
