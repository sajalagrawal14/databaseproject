package com.example.database;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

//Класс таблицы
public class AlertDialogBruh extends DialogFragment {

    String message;

    //Кастомный конструктор
    public AlertDialogBruh (String message) {
        this.message = message;
    }

    //Создание AlertDialog'а
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Случайное желание");  // заголовок
        builder.setMessage(message); // сообщение

        //Позитивная кнопка "Добавить"
        builder.setPositiveButton("Добавить", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Вызываем метод записи данных
                MainActivity.onItemClicked(message);
            }
        });
        //Негативная кнопка "Удалить"
        builder.setNegativeButton("Удалить", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(getActivity(), "OK", Toast.LENGTH_LONG).show();
            }
        });
        builder.setCancelable(true);

        return builder.create();
    }
}
